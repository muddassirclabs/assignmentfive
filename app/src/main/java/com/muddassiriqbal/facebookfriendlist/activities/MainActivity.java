package com.muddassiriqbal.facebookfriendlist.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.muddassiriqbal.facebookfriendlist.R;
import com.muddassiriqbal.facebookfriendlist.util.Data;
import com.muddassiriqbal.facebookfriendlist.util.MainAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.*;
import static com.muddassiriqbal.facebookfriendlist.util.Data.*;


public class MainActivity extends FragmentActivity {
    String url, friendID, friendName;
    JSONObject mainData,friend;
    JSONArray friendList;
    HashMap<String,String> friendListMap = new HashMap<>();
    ListView listView;
    ProgressDialog progress;
    private MainFragment mainFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.list_view);
        progress = new ProgressDialog(this);
        if (savedInstanceState == null) {
            // Add the fragment on initial activity setup
            mainFragment = new MainFragment();

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.content, mainFragment)
                    .commit();
        } else {
            // Or set the fragment from restored state info
            mainFragment = (MainFragment) getSupportFragmentManager()
                    .findFragmentById(android.R.id.content);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                friendID = friendListMap.get(list.get(position));
                Intent intent = new Intent(MainActivity.this,ProfileView.class);
                intent.putExtra(ID_KEY,friendID);
                startActivity(intent);
            }
        });

    }

    public void onClick(View v){
        switch (v.getId()){
            case R.id.fetch_list_btn:
                getFriendList();
                break;
        }
    }

    public void getFriendList(){
        url = MY_FRIEND_LIST_URL + Data.ACCESS_TOKEN;
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler(){

            @Override
            public void onSuccess(String content) {
                try{
                    mainData = new JSONObject(content);
                    friendList = mainData.getJSONArray(DATA_KEY);
                    for (int i = 0; i < friendList.length(); i++){

                        friend = friendList.getJSONObject(i);
                        friendName = friend.getString(NAME_KEY);
                        friendID = friend.getString(ID_KEY);

                        // Getting List of Friends and setting it in the adapter
                        list = new ArrayList<>(friendListMap.keySet());
                        friendListMap.put(friendName, friendID);
                        adapter = new MainAdapter(MainActivity.this,list);
                        listView.setAdapter(adapter);
                        listView.setVisibility(View.VISIBLE);
                    }


                }catch (Exception e){
                    Toast.makeText(MainActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
                }
                progress.dismiss();
            }

            @Override
            public void onFailure(Throwable error,String s) {
                progress.dismiss();
                Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle(FAILURE_DIALOG_MESSAGE);
                dialog.show();
            }
        });
    }
}
