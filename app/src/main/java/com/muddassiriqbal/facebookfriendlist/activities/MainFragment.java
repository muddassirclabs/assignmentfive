package com.muddassiriqbal.facebookfriendlist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.muddassiriqbal.facebookfriendlist.R;
import com.muddassiriqbal.facebookfriendlist.util.Data;

import java.util.Arrays;

import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.APP_ID;
import static com.muddassiriqbal.facebookfriendlist.util.Data.adapter;
import static com.muddassiriqbal.facebookfriendlist.util.Data.list;


public class MainFragment extends android.support.v4.app.Fragment {
    private static final String TAG = "MainFragment";
    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        com.facebook.Settings.setApplicationId(APP_ID);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main, container, false);
        LoginButton authButton = (LoginButton) view.findViewById(R.id.authButton);
        authButton.setReadPermissions(Arrays.asList("user_about_me", "user_friends"));

        authButton.setFragment(this);

        return view;
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            Log.d(TAG, "Logged in...");
            Data.ACCESS_TOKEN = session.getAccessToken();

        } else if (state.isClosed()) {
            Log.d(TAG, "Logged out...");
            if (!list.isEmpty() && !adapter.isEmpty()) {
                list.clear();
                adapter.notifyDataSetChanged();
            }
            session.closeAndClearTokenInformation();
        }

    }

    @Override
    public void onResume() {
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }
}
