package com.muddassiriqbal.facebookfriendlist.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.muddassiriqbal.facebookfriendlist.R;
import com.muddassiriqbal.facebookfriendlist.util.Data;

import org.json.JSONObject;

import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.FAILURE_DIALOG_MESSAGE;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.FIRST_NAME_KEY;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.F_NAME_INDEX;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.GENDER_INDEX;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.GENDER_KEY;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.GRAPH_API_URL;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.ID_KEY;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.LAST_NAME_KEY;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.LINK_INDEX;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.LINK_KEY;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.L_NAME_INDEX;
import static com.muddassiriqbal.facebookfriendlist.util.AppConstant.QUERY;

public class ProfileView extends Activity {

    TextView firstName, lastName, gender, link;
    String[] detail = new String[4];
    String friendID = "";
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);
        progress = new ProgressDialog(this);

        friendID = getIntent().getStringExtra(ID_KEY);

        firstName = (TextView) findViewById(R.id.first_name);
        lastName = (TextView) findViewById(R.id.last_name);
        gender = (TextView) findViewById(R.id.gender);
        link = (TextView) findViewById(R.id.link);

        extractDetails();
    }

    public void extractDetails() {
        String url = GRAPH_API_URL + friendID + QUERY + Data.ACCESS_TOKEN;
        progress.setTitle("Fetching Friend's Details...");
        progress.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String content) {
                try {
                    JSONObject obj = new JSONObject(content);
                    detail[F_NAME_INDEX] = obj.getString(FIRST_NAME_KEY);
                    detail[L_NAME_INDEX] = obj.getString(LAST_NAME_KEY);
                    detail[GENDER_INDEX] = obj.getString(GENDER_KEY);
                    detail[LINK_INDEX] = obj.getString(LINK_KEY);

                    firstName.setText(detail[F_NAME_INDEX]);
                    lastName.setText(detail[L_NAME_INDEX]);
                    gender.setText(detail[GENDER_INDEX]);
                    link.setText(Html.fromHtml("<b><a href=\"" + detail[LINK_INDEX] + "\">Click Here</a></b>"));
                    link.setMovementMethod(LinkMovementMethod.getInstance());

                    progress.dismiss();

                } catch (Exception e) {
                    Toast.makeText(ProfileView.this, e.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable error, String s) {
                progress.dismiss();
                Dialog dialog = new Dialog(ProfileView.this);
                dialog.setTitle(FAILURE_DIALOG_MESSAGE);
                dialog.show();
            }
        });
    }
}
