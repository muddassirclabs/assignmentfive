package com.muddassiriqbal.facebookfriendlist.util;

public interface AppConstant {
    //String ACCESS_TOKEN = "CAACEdEose0cBAGHzgwOwrUraLcOdFH192Scm5LZC7uN2VIgpBgZB5Ga7qTEF4AoSUqVuYL2hoiSxCKx8jwqWA9YDazd4zCGggKDe6Ocg752pOIV749LV8LVO1gfHIGzt1AQzLE2oIuCPNmCcTU3mT4ChgDzHO8gasjn00PS7j5JdcOND81ErIXXps1ZB8tMTvh37Nx7kFBinn0sYrn86dbbFD2FiocZD";
    String MY_FRIEND_LIST_URL = "https://graph.facebook.com/me/friends?access_token=";
    String GRAPH_API_URL = "https://graph.facebook.com/";
    String QUERY = "?access_token=";
    String FAILURE_DIALOG_MESSAGE = "Connection to server Failed! Please Login to Facebook Account First.";
    String APP_ID = "417995415021905";
    String NEW_LINE = System.getProperty("line.separator");

    int F_NAME_INDEX = 0;
    int L_NAME_INDEX = 1;
    int GENDER_INDEX = 2;
    int LINK_INDEX = 3;

    String DATA_KEY = "data";
    String NAME_KEY = "name";
    String ID_KEY = "id";
    String FIRST_NAME_KEY = "first_name";
    String LAST_NAME_KEY = "last_name";
    String GENDER_KEY = "gender";
    String LINK_KEY = "link";
    String FRIEND_KEY = "Friend Detail";

}
