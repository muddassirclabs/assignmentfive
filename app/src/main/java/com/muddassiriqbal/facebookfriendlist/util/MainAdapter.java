package com.muddassiriqbal.facebookfriendlist.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.muddassiriqbal.facebookfriendlist.R;

import java.util.List;

public class MainAdapter extends BaseAdapter{
    List<String> data;
    Context context;

    public MainAdapter(Context ctx, List<String> data){
        this.data = data;
        this.context = ctx;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_main_adapter,null);
        TextView itemName = (TextView)view.findViewById(R.id.student_name);
        itemName.setText(data.get(position));
        return view;
    }
}
